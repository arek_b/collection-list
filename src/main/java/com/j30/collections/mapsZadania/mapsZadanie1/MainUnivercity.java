package com.j30.collections.mapsZadania.mapsZadanie1;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Long, Student> students = new HashMap();
        Student student1 = new Student(100300L, "Paweł", "Lis");
        students.put(student1.getNrIndeksu(), student1);
        Student student2 = new Student(100400L, "Gaweł", "Cis");
        students.put(student2.getNrIndeksu(), student2);
        Student student3 = new Student(100500L, "Piotr", "Nis");
        students.put(student3.getNrIndeksu(), student3);
        Student student4 = new Student(100600L, "Michał", "Dis");
        students.put(student4.getNrIndeksu(), student4);
        Student student5 = new Student(100200L, "Jacek", "Kis");
        students.put(student5.getNrIndeksu(), student5);
        if (students.containsKey(100400L)) {
            System.out.println("Zawiera indeks 100400");
        }else{
            System.out.println("NIe zawiera indeksu 100400.");
        }
        System.out.println(students.get(100300L));
        System.out.println(students.get(100301L));
        System.out.println(students.size());
        System.out.println("Pętla iterująca wartości:");
        for (Student value : students.values()){
            System.out.println(value);
        }
    }

}


