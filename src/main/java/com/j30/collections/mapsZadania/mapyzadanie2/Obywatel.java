package com.j30.collections.mapyzadanie2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@Slf4j
public class Obywatel {
    String pesel;
    String imie;
    String nazwisko;
}
