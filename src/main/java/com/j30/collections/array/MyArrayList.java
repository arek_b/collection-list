package com.j30.collections.array;

public class MyArrayList {
    private static final int INITIAL_ARRAY_SIZE = 10;
    private Object[] array;
    private int size = 0; //rozmiar listy na początku

    public MyArrayList() {
        this(INITIAL_ARRAY_SIZE);
    }
    public MyArrayList(int initialArraySize) {
        array = new Object[initialArraySize];
    }

    //todo:
    //  -dodawanie elemetów
    //  -listownie elementów
    //  -size
    //  -dodanie elementu na pozycje
    //  -usunięcie elementu z pozycji n

    public void add(Object element) {
        checkSizeAndExtendIfNeeded();
        array[size++] = element;
    }
    public int size() {
        return size;
    }
    public void remove (int indeks){
        if (indeks >= 0 && indeks < size){
            for (int i = indeks; i < size - 1; i++) {
                array[i] = array[i+1];
            }
            array[--size] = null;
        }else{
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }
    }

    private void checkSizeAndExtendIfNeeded() {
        if (size >= array.length){
            //1.stworzyć tablicę 2 razy większą
            Object[] newArray = new Object [array.length * 2];
            //2. przepisanie elementów
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
                //3.
            }
            array= newArray; // nie intersuje mnie stara lista tylko nowa;
        }
    }

@Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                sb.append(array[i]);
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
