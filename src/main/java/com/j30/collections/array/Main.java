package com.j30.collections.array;

public class Mian
{
    public static void main(String[] args) {
        MyArrayList<Double> list = new MyArrayList<Double>(100);
//        list.add("abc");
//        list.add("abc");
//        list.add("def");
//        list.add(1);
//        list.add(1);
        list.add(5.0);
        list.add(0.3);
        list.add(0.4);
        list.add(0.5);
        list.add(0.6);
        list.add(0.7);
        list.add(0.8);
        System.out.println(list);
        list.remove(5);
        System.out.println(list);
        list.remove(0);
        System.out.println(list);
        list.remove(list.size()-1);
        System.out.println(list);
        while(list.size()>0){
            list.remove(0);
            System.out.println(list);
        }
        list.add(0, 5.0 );
        list.add(1, 4.0 );
        list.add(2, 3.0 );
        list.add(3, 2.0 );
        list.add(4, 1.0 );
        System.out.println(list);
        list.add(3,0.4);
        System.out.println(list);

    }
}

