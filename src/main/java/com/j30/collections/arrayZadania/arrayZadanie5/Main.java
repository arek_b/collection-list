package com.j30.collections.arrayZadanie5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main
{
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.addAll(Arrays.asList(
                new Student("120316", "Arkadiusz", "Bis", Gender.MEZCZYZNA),
                new Student("120314", "Kasia", "Lis", Gender.KOBIETA)
        ));

    }
}
