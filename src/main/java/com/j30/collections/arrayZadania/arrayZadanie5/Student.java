package com.j30.collections.arrayZadanie5;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Student {
    private String nrIndeksu;
    private String imie;
    private String nazwisko;
    private Gender plec;

    public Student(String nrIndeksu, String imie, String nazwisko, Gender plec) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;

    }
}

