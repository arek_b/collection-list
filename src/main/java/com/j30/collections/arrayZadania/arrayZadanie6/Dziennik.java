package com.j30.collections.arrayZadanie6;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Dziennik {
    private List<Student> students = new ArrayList<>();


    public void dodajStudenta(Student student) {
        students.add(student);
    }

    public void usunStudenta(Student student) {
        students.remove(student);
    }

    public void usunStudenta(String indeksStudenta) {
        List<Student> kopiaListaIterowna = new ArrayList<>(students);
        for (Student student : kopiaListaIterowna) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeksStudenta)) {
                students.remove(student);
                break;
            }
        }
    }

    public Optional<Student> zwrocStudenta(String indeksStudneta) {
        for (Student student : students) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeksStudneta)) {
                students.remove(student);
                return Optional.of(student);
            }
        }
        return Optional.empty();
    }
//    public double podajSredniaStudenta(String indeksStudenta) {
//        Optional<Student> studentOptional= zwrocStudenta(indeksStudenta);
//        if(studentOptional.isPresent()){
//            Student student = studentOptional
//        }
//    }
}
