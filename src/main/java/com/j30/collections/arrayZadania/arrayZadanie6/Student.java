package com.j30.collections.arrayZadanie6;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
@Data
@AllArgsConstructor

public class Student {
    String nrIndeksu;
    String imie;
    String nazwisko;
    List<Double> listOcen;

    public List<Double> getListOcen() {
        return listOcen;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
